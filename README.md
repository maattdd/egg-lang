# Disclaimer

⚠️ This is completely work-in-progress, don't expect anything to work ⚠️

# Inspiration

* From procedural programming: statements, assignments.
* From object-oriented programming: dot syntax, late binding, overloading, multiple dispatch.
* From FP: sum type (ADT), no null, value type, first class functions, lambdas.

The main goal is to have the simplicity of FP, but with the discoverabilty of OOP by mixing UFCS, pipelining and structures.
Almost all features are designed with the programmer in mind, and that's the reason this repo contains also an LSP server.

# TODO

* Multiline comment
* Fix module loading
* if/else expr
* Array
* Dict
* Formatter
* UFCS
* Struct type (product type) + struct literal
* Named parameter
* Polymorphism (multiple dispatch)
* Sum type ( Foo | Bar)
* Lambdas

## Planned

* Dynamic scoped (=implicit) variable
* Annotations
* Local type inference (HM?)
* LSP : handle bloc granularity

# VSCode

You can install the VSCode extension to activate the LSP server: https://marketplace.visualstudio.com/items?itemName=MatthieuDubet.vscode-egg-lsp