use clap::{AppSettings, Clap};
use egg::ast::*;
use egg::compile::*;
use egg::parser::*;

/// Welcome to the egg-lang binary.
#[derive(Clap)]
#[clap(setting = AppSettings::ColoredHelp)]
struct Opts {
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
    Compile,
    Eval,
    // Read Eval Print Loop
    Repl,
    // LSP server for code editor
    Lsp,
}

fn main() {
    let opts: Opts = Opts::parse();
    match opts.subcmd {
        SubCommand::Compile => {
            println!("Compiling...");
            let module = parse_file("simple");
            let compiled = compile_module(module);
            println!("{:?}", compiled);
        }
        SubCommand::Eval => {
            let module = parse_file("example");
            println!("{:?}", module);
            let evaluated = module.evaluate();
            println!("{:?}", evaluated);
        }
        SubCommand::Lsp => {
            egg::lsp::main();
        }
        SubCommand::Repl => {
            use rustyline::error::ReadlineError;
            use rustyline::Editor;
            let mut rl = Editor::<()>::new();
            let history_file = "~/.egg_repl_history";
            if rl.load_history(history_file).is_err() {
                println!("No previous history.");
            }
            let mut stack = egg::evaluate::new_stack();
            loop {
                let readline = rl.readline(">> ");
                match readline {
                    Ok(line) => {
                        rl.add_history_entry(line.as_str());
                        let parsed = parse_statement(line.as_str());
                        let result = match parsed {
                            Some(Parsed::Valid(stmt, _)) => {
                                let res = stmt.evaluate(&mut stack);
                                match res {
                                    Ok(Some(v)) => Some(v),
                                    _ => None,
                                }
                            }
                            _ => None,
                        };
                        println!("{:?}", result);
                    }
                    Err(ReadlineError::Interrupted) => {
                        println!("CTRL-C");
                        break;
                    }
                    Err(ReadlineError::Eof) => {
                        println!("CTRL-D");
                        break;
                    }
                    Err(err) => {
                        println!("Error: {:?}", err);
                        break;
                    }
                }
            }
            rl.save_history(history_file).unwrap();
        }
    }
}
