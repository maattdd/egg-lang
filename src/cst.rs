use crate::lex;
use std::fmt;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Token {
	pub begin_line: u32,
	pub begin_pos: u32,
	pub end_line: u32,
	pub end_pos: u32,
	pub size: u32,
	// actual content
	pub kind: Kind,
	pub t: String,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Kind {
	Module,
	Func,
	Return,
	Struct,
	LeftBracket,
	RightBracket,
	LeftParenthesis,
	RightParenthesis,
	Star,
	Plus,
	LessThan,
	MoreThan,
	Assign,
	Id,
	NamedId,
	Sharp,
	Semicolon,
	IntegerLiteral,
	StringLiteral,
	Comma,
	Colon,
	Dot,
	Comment,
	Error(String),
}

impl fmt::Display for Token {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.t)
	}
}

#[derive(Debug, Clone)]
pub struct Tk {
	line: u32,
	pos: u32,
	t: String,
}
impl Tk {
	fn size(&self) -> u32 {
		return self.t.len() as u32;
	}
}

fn tk_to_token(tk: &Tk, kind: Kind) -> Token {
	return Token {
		kind,
		begin_line: tk.line,
		begin_pos: tk.pos,
		end_line: tk.line, // single line token
		end_pos: tk.pos + tk.size(),
		size: tk.size(),
		t: tk.t.clone(),
	};
}

pub fn process(content: &str) -> Vec<Token> {
	use logos::Logos;
	let mut l = crate::lex::Token::lexer(content);
	return process_tokens(&mut l);
}

struct State {
	ret: Vec<Token>,
	content: String,
	multiline_comments_stack: Vec<Tk>,
	line: u32,
	pos: u32,
}

fn process_tokens(input: &mut logos::Lexer<lex::Token>) -> Vec<Token> {
	// handle correctly balanced multiline comments
	let mut state = State {
		multiline_comments_stack: Vec::new(),
		content: String::from(""),
		ret: Vec::new(),
		line: 0,
		pos: 0,
	};
	let to_token = |state: &mut State, tk: Tk, kind| {
		if state.multiline_comments_stack.len() > 0 {
			state.content.push_str(&tk.t.to_string());
		} else {
			state.ret.push(tk_to_token(&tk, kind))
		}
		state.pos += tk.size();
	};
	loop {
		let it = input.next();
		if it == None {
			break;
		};
		let t = input.slice().to_string();
		let tk = Tk {
			line: state.line,
			pos: state.pos,
			t,
		};
		match it.unwrap() {
			lex::Token::BeginMultilineComment => {
				state.multiline_comments_stack.push(tk);
			}
			lex::Token::EndMultilineComment => {
				if state.multiline_comments_stack.len() == 1 {
					let begin = state.multiline_comments_stack.pop().unwrap();
					// its a new comment
					state.ret.push(Token {
						begin_line: begin.line,
						begin_pos: begin.pos,
						end_line: state.line,
						end_pos: state.pos,
						size: state.content.len() as u32,
						kind: Kind::Comment,
						t: state.content.to_string(),
					});
					state.content = String::from("");
				} else if state.multiline_comments_stack.len() < 1 {
					state.ret.push(Token {
						kind: Kind::Error(format!(
							"unmatched comment closing"
						)),
						begin_line: tk.line,
						begin_pos: tk.pos,
						end_line: tk.line, // single line token
						end_pos: tk.pos + tk.size(),
						size: tk.size(),
						t: tk.t.clone(),
					});
				} else {
					state.multiline_comments_stack.pop();
				}
			}
			lex::Token::Module => to_token(&mut state, tk, Kind::Module),
			lex::Token::Func => to_token(&mut state, tk, Kind::Func),
			lex::Token::Return => to_token(&mut state, tk, Kind::Return),
			lex::Token::Struct => to_token(&mut state, tk, Kind::Struct),
			lex::Token::LeftBracket => to_token(&mut state, tk, Kind::LeftBracket),
			lex::Token::RightBracket => to_token(&mut state, tk, Kind::RightBracket),
			lex::Token::LeftParenthesis => {
				to_token(&mut state, tk, Kind::LeftParenthesis)
			}
			lex::Token::RightParenthesis => {
				to_token(&mut state, tk, Kind::RightParenthesis)
			}
			lex::Token::LessThan => to_token(&mut state, tk, Kind::LessThan),
			lex::Token::MoreThan => to_token(&mut state, tk, Kind::MoreThan),
			lex::Token::Assign => to_token(&mut state, tk, Kind::Assign),
			lex::Token::Id => to_token(&mut state, tk, Kind::Id),
			lex::Token::Sharp => to_token(&mut state, tk, Kind::Sharp),
			lex::Token::NamedId => to_token(&mut state, tk, Kind::NamedId),
			lex::Token::Semicolon => to_token(&mut state, tk, Kind::Semicolon),
			lex::Token::IntegerLiteral => {
				to_token(&mut state, tk, Kind::IntegerLiteral)
			}
			lex::Token::StringLiteral => to_token(&mut state, tk, Kind::StringLiteral),
			lex::Token::LineComment => to_token(&mut state, tk, Kind::Comment),
			lex::Token::Star => to_token(&mut state, tk, Kind::Star),
			lex::Token::Plus => to_token(&mut state, tk, Kind::Plus),
			lex::Token::Comma => to_token(&mut state, tk, Kind::Comma),
			lex::Token::Colon => to_token(&mut state, tk, Kind::Colon),
			lex::Token::Dot => to_token(&mut state, tk, Kind::Dot),
			lex::Token::NewLine => {
				state.line += 1;
				state.pos = 0;
				if state.multiline_comments_stack.len() > 0 {
					state.content.push_str(&tk.t.to_string());
				}
			}
			lex::Token::Space => {
				state.pos += 1;
				if state.multiline_comments_stack.len() > 0 {
					state.content.push_str(&tk.t.to_string());
				}
			}
			lex::Token::Anything => {
				if state.multiline_comments_stack.len() > 0 {
					state.content.push_str(&tk.t.to_string());
				} else {
					state.ret.push(Token {
						kind: Kind::Error(format!(
							"unexpected token {:?}",
							tk.t
						)),
						begin_line: tk.line,
						begin_pos: tk.pos,
						end_line: tk.line, // single line token
						end_pos: tk.pos + tk.size(),
						size: tk.size(),
						t: tk.t.clone(),
					});
				}
			}
			lex::Token::Error => panic!("unknown internal error"),
		}
	}
	if state.multiline_comments_stack.len() > 0 {
		let tk = state.multiline_comments_stack.pop().unwrap();
		state.ret.push(Token {
			kind: Kind::Error(format!("unmatched comment opening")),
			begin_line: tk.line,
			begin_pos: tk.pos,
			end_line: tk.line, // single line token
			end_pos: tk.pos + tk.size(),
			size: tk.size(),
			t: tk.t.clone(),
		});
	}
	return state.ret;
}

#[test]
fn test_lexer() {
	let tokens = process("module // test \n /*& comment\nmulti*/");
	assert_eq!(
		tokens[2],
		Token {
			begin_pos: 1,
			begin_line: 1,
			end_line: 2,
			end_pos: 5,
			size: 15,
			kind: Kind::Comment,
			t: String::from("& comment\nmulti")
		}
	);
}
