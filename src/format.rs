use crate::ast::*;

fn indent(input: String, space: &str) -> String {
	let indented: Vec<String> = input
		.split("\n")
		.map(|s| format!("{}{}", space, s))
		.collect();
	return indented.join("\n");
}

pub trait Formatable {
	fn to_format(&self, space: &str) -> String;
}

impl Formatable for FileModule {
	fn to_format(&self, space: &str) -> String {
		return self.module_content.to_format(space);
	}
}

impl Formatable for Struct {
	fn to_format(&self, _space: &str) -> String {
		return String::from("todo");
	}
}
impl Formatable for StructLiteralExpression {
	fn to_format(&self, space: &str) -> String {
		let mapped_assigns: Vec<String> =
			self.assigns.iter().map(|s| s.to_format(space)).collect();
		let joined = mapped_assigns.join("\n");
		return format!(
			"{}\n{}\n{}",
			self.left_bracket.to_format(space),
			indent(joined, space),
			self.right_bracket.to_format(space)
		);
	}
}
impl Formatable for Expression {
	fn to_format(&self, space: &str) -> String {
		return match self {
			Expression::Identifier(s) => format!("{}", s),
			Expression::StringLiteral(s) => format!("{}", s),
			Expression::IntLiteral(i) => format!("{}", i),
			Expression::StructLiteral(i) => format!("{}", i.to_format(space)),
			Expression::Plus(a, b) => {
				format!("{} + {}", a.to_format(space), b.to_format(space))
			}
			Expression::Dot(a, b) => {
				format!("{}.{}", a.to_format(space), b.to_format(space))
			}
			Expression::Multiply(a, b) => {
				format!("{} * {}", a.to_format(space), b.to_format(space))
			}
			Expression::Evaluate(a) => {
				format!("#{}", a.expression.to_format(space))
			}
			Expression::Call(a, args) => {
				format!("{}({})", a.to_format(space), args[0].to_format(space))
			}
		};
	}
}
impl Formatable for Assign {
	fn to_format(&self, space: &str) -> String {
		return format!(
			"{} {} {}",
			self.name.to_format(space),
			self.assign_symbol.to_format(space),
			self.expression.to_format(space)
		);
	}
}
impl Formatable for Statement {
	fn to_format(&self, space: &str) -> String {
		return match self {
			Statement::Return(r, exp, semicolon) => {
				format!(
					"{} {}{}",
					r.to_format(space),
					exp.to_format(space),
					semicolon.to_format(space)
				)
			}
			Statement::StmtAssign(assign, semicolon) => {
				format!("{}{}", assign.to_format(space), semicolon.to_format(space))
			}

			Statement::Void(exp, semicolon) => {
				format!("{}{}", exp.to_format(space), semicolon.to_format(space))
			}
		};
	}
}
impl Formatable for crate::cst::Token {
	fn to_format(&self, _space: &str) -> String {
		return self.t.clone();
	}
}
impl Formatable for Module {
	fn to_format(&self, space: &str) -> String {
		let content = indent(self.module_content.to_format(space), space);
		return format!(
			"{} {} {}\n{}\n{}\n",
			self.module_keyword.to_format(space),
			self.name.to_format(space),
			self.left_bracket.to_format(space),
			content,
			self.right_bracket.to_format(space)
		);
	}
}
impl Formatable for Parameter {
	fn to_format(&self, space: &str) -> String {
		return format!(
			"{}{}{}",
			self.name.to_format(space),
			self.colon.to_format(space),
			self.ttype.to_format(space)
		);
	}
}
impl Formatable for Function {
	fn to_format(&self, space: &str) -> String {
		let mapped_statements: Vec<String> =
			self.statements.iter().map(|s| s.to_format(space)).collect();
		let mapped_parameters: Vec<String> =
			self.parameters.iter().map(|s| s.to_format(space)).collect();
		return format!(
			"{} {} {}{}{} {}\n{}\n{}\n",
			self.func_symbol.to_format(space),
			self.name.to_format(space),
			self.left_parenthesis.to_format(space),
			mapped_parameters.join(", "),
			self.right_parenthesis.to_format(space),
			self.left_bracket.to_format(space),
			indent(mapped_statements.join("\n"), space),
			self.right_bracket.to_format(space),
		);
	}
}
impl Formatable for ModuleElement {
	fn to_format(&self, space: &str) -> String {
		return match self {
			ModuleElement::Function(f) => (*f).to_format(space),
			ModuleElement::Module(m) => (*m).to_format(space),
			ModuleElement::Struct(s) => (*s).to_format(space),
		};
	}
}
impl Formatable for ModuleContent {
	fn to_format(&self, space: &str) -> String {
		let mapped: Vec<String> =
			self.elements.iter().map(|e| e.to_format(space)).collect();
		return mapped.join("");
	}
}
impl<T: Formatable> Formatable for Parsed<T> {
	fn to_format(&self, space: &str) -> String {
		return match self {
			Parsed::Valid(x, comments) => {
				let mapped_comments: Vec<String> =
					comments.iter().map(|e| e.to_format(space)).collect();
				format!("{}{}", x.to_format(space), mapped_comments.join("\n"))
			}
			_ => format!("todo"),
		};
	}
}
