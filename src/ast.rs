use crate::cst::Token;

pub trait ModuleT {
    fn name(&self) -> String;
}
#[derive(Debug, Clone)]
pub struct ModuleContent {
    pub elements: Vec<ModuleElement>,
}

#[derive(Debug, Clone)]
pub struct Module {
    pub module_keyword: Parsed<Token>,
    pub name: Parsed<Token>,
    pub module_content: Parsed<ModuleContent>,
    pub left_bracket: Parsed<Token>,
    pub right_bracket: Parsed<Token>,
}
impl ModuleT for Module {
    fn name(&self) -> String {
        return self.name.unwrap().t;
    }
}
#[derive(Debug, Clone)]
pub struct FileModule {
    pub module_content: Parsed<ModuleContent>,
    pub name: String,
}
impl ModuleT for FileModule {
    fn name(&self) -> String {
        return self.name.clone();
    }
}
impl FileModule {
    pub fn get_main(&self) -> Result<Function, String> {
        return self
            .module_content
            .unwrap()
            .functions()
            .into_iter()
            .map(|x| x.unwrap())
            .find(|x| x.name.clone().unwrap().t == "main")
            .ok_or("no main function".to_string());
    }
}
#[derive(Debug, Clone)]
pub struct Struct {
    pub keyword: Parsed<Token>,
    pub name: Parsed<Token>,
    pub left_bracket: Parsed<Token>,
    pub right_bracket: Parsed<Token>,
}

#[derive(Debug, Clone)]
pub enum ModuleElement {
    Function(Box<Parsed<Function>>),
    Module(Box<Parsed<Module>>),
    Struct(Box<Parsed<Struct>>),
}
impl ModuleContent {
    pub fn functions(&self) -> Vec<Parsed<Function>> {
        return self
            .elements
            .iter()
            .filter_map(|x| match x {
                ModuleElement::Function(f) => Some(*f.clone()),
                _ => None,
            })
            .collect();
    }
    pub fn modules(&self) -> Vec<Parsed<Module>> {
        return self
            .elements
            .iter()
            .filter_map(|x| match x {
                ModuleElement::Module(f) => Some(*f.clone()),
                _ => None,
            })
            .collect();
    }
}

#[derive(Debug, Clone)]
pub struct Parameter {
    pub named: bool,
    pub name: Parsed<Token>,
    pub colon: Parsed<Token>,
    pub ttype: Parsed<Token>,
    pub comma: Option<Parsed<Token>>,
}

#[derive(Debug, Clone)]
pub struct Function {
    pub func_symbol: Parsed<Token>,
    pub name: Parsed<Token>,
    //pub return_type: Option<String>,
    //pub generics: Vec<Parameter>,
    pub parameters: Vec<Parsed<Parameter>>,
    pub statements: Vec<Parsed<Statement>>,
    pub left_parenthesis: Parsed<Token>,
    pub right_parenthesis: Parsed<Token>,
    pub left_bracket: Parsed<Token>,
    pub right_bracket: Parsed<Token>,
}

#[derive(Debug, Clone)]
pub enum Statement {
    // return 4;
    Return(Parsed<Token>, Parsed<Expression>, Parsed<Token>),
    // print();
    Void(Parsed<Expression>, Parsed<Token>),
    // x := 4;
    StmtAssign(Parsed<Assign>, Parsed<Token>),
}

#[derive(Debug, Clone)]
pub struct Assign {
    pub name: Parsed<Token>,
    pub assign_symbol: Parsed<Token>,
    pub expression: Parsed<Expression>,
}

#[derive(Debug, Clone)]
pub struct FakeToken {
    pub start_line: u32,
    pub start_pos: u32,
    pub end_line: u32,
    pub end_pos: u32,
}

#[derive(Debug, Clone)]
pub enum Parsed<T> {
    // valid contains the actual AST node, and a potential list of comment tokens
    Valid(T, Vec<Token>),
    // invalid contains an error string and all the "swallowed" tokens
    Invalid(String, Vec<Token>),
    InvalidEOF,
}

impl<T: Clone + std::fmt::Debug> Parsed<T> {
    pub fn unwrap(&self) -> T {
        match self {
            Parsed::Valid(x, _) => {
                return x.clone();
            }
            _ => panic!("can't unwrap {:?}", self),
        }
    }
}

#[derive(Debug, Clone)]
pub struct StructLiteralExpression {
    pub left_bracket: Parsed<Token>,
    pub right_bracket: Parsed<Token>,
    pub assigns: Vec<Parsed<Assign>>,
}

#[derive(Debug, Clone)]
pub struct EvaluateExpression {
    pub sharp: Token,
    pub expression: Box<Parsed<Expression>>,
    pub evaluated: Option<Box<Parsed<Expression>>>,
}

#[derive(Debug, Clone)]
pub enum Expression {
    Identifier(Token),
    StringLiteral(Token),
    StructLiteral(StructLiteralExpression),
    IntLiteral(Token),
    Dot(Box<Parsed<Expression>>, Token),
    Plus(Box<Parsed<Expression>>, Box<Parsed<Expression>>),
    Multiply(Box<Parsed<Expression>>, Box<Parsed<Expression>>),
    Evaluate(EvaluateExpression),
    Call(Box<Parsed<Expression>>, Vec<Parsed<Expression>>),
}
impl Expression {
    pub fn to_sexp(&self) -> String {
        match self {
            Expression::Plus(a, b) => {
                format!("(+ {} {})", a.unwrap().to_sexp(), b.unwrap().to_sexp())
            }
            Expression::Multiply(a, b) => {
                format!("(* {} {})", a.unwrap().to_sexp(), b.unwrap().to_sexp())
            }
            Expression::Identifier(a) => format!("{}", a.t),
            Expression::StringLiteral(a) => format!("{}", a.t),
            Expression::IntLiteral(a) => format!("{}", a.t),
            Expression::Evaluate(a) => format!("(# {})", a.expression.unwrap().to_sexp()),
            Expression::Dot(a, b) => format!("(. {} {})", a.unwrap().to_sexp(), b.t),
            Expression::Call(a, args) => format!(
                "(call {} {})",
                a.unwrap().to_sexp(),
                args[0].unwrap().to_sexp()
            ),
            s => format!("{:?}", s),
        }
    }
}

pub trait Compilable {
    fn compile(&self) -> Result<String, String>;
}
