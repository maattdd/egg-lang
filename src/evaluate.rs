use crate::ast::*;
use std::collections::HashMap;
use std::fmt;

type Fields = HashMap<String, Value>;

#[derive(Debug, Clone)]
pub enum Value {
    Void,
    Str(String),
    Int32(i32),
    Strct(Fields),
    LibFn(&'static str),
    UserFn(Function),
    UserModule(Module),
}
#[derive(Debug, Clone)]
pub enum Type {
    Trait(),
    Strct(Fields),
    String,
    Int32,
    Module,
}

/* STDLIB FUNCTIONS */
pub fn exec(args: &Vec<Value>) -> Value {
    use std::process::Command;
    let str = args[0].get_str().unwrap();
    let parts: Vec<&str> = str.split(' ').collect();
    let cmd_name = parts[0];
    //println!("cmd: {}", cmd_name);
    let first_arg = parts[1];
    //println!("first arg: {}", first_arg);
    let output = Command::new(cmd_name)
        .arg(first_arg)
        .output()
        .expect("failed to run");
    let s = String::from_utf8_lossy(&output.stdout).to_string();
    //println!("cmd result: {}", s);
    return Value::Str(s);
}
pub fn debug(x: &Value) -> Value {
    println!("{:?}", x);
    return Value::Void;
}
pub fn print(args: &Vec<Value>) -> Value {
    println!("{:?}", args);
    return Value::Void;
}

/* DEBUGGING FUNCTIONS */
impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Value::Void => write!(f, ""),
            Value::Str(x) => write!(f, "{}", x),
            z => write!(f, "{:?}", z),
        }
    }
}

impl Value {
    pub fn get_str(&self) -> Result<String, String> {
        return match self {
            Value::Str(s) => Ok(s.to_string()),
            _ => Err("todo: not a string".to_string()),
        };
    }
}

type Symbols = HashMap<String, Value>;
pub struct SStack(Vec<Symbols>);
pub fn new_stack<'a>() -> SStack {
    let stack: SStack = SStack(Vec::<Symbols>::new());
    return stack;
}
impl SStack {
    fn insert_symbol(&mut self, k: &str, v: Value) {
        self.0
            .last_mut()
            .map(|table| table.insert(k.to_string(), v));
    }
    fn remove_last_table(&mut self) {
        let idx = self.0.len();
        self.0.remove(idx - 1);
    }
    fn deep_find(&self, sym: &str) -> Result<Value, String> {
        for table in self.0.iter().rev() {
            let founded = table.get(sym);
            match founded {
                Some(x) => {
                    return Ok(x.clone());
                }
                None => {}
            }
        }
        return Err(format!("unknown symbol {}", sym));
    }
}

impl Function {
    fn evaluate<'a>(&'a self, args: Vec<Value>, stack: &'a mut SStack) -> Result<Value, String> {
        let mut locals = Symbols::new();
        // add argument to symbol map
        for it in self.parameters.iter().zip(args.iter()) {
            let (ai, bi) = it;
            //println!("inserting symbol {}", ai);
            locals.insert(ai.unwrap().name.unwrap().t.to_string(), bi.clone());
        }
        stack.0.push(locals);
        // execute statements
        for s in &self.statements {
            s.unwrap().evaluate(stack)?;
        }
        stack.remove_last_table();
        return Ok(Value::Void);
    }
}

impl Statement {
    pub fn evaluate(&self, stack: &mut SStack) -> Result<Option<Value>, String> {
        return match self {
            Statement::Return(_, r, _) => {
                let ret = r.clone().unwrap().evaluate(stack).unwrap();
                stack.remove_last_table();
                Ok(Some(ret))
            }
            Statement::StmtAssign(x, _) => {
                let rhs = x.clone().unwrap().expression.unwrap().evaluate(stack)?;
                stack.insert_symbol(x.clone().unwrap().name.unwrap().t.as_str(), rhs);
                Ok(None)
            }
            Statement::Void(e, _) => {
                e.clone().unwrap().evaluate(stack)?;
                Ok(None)
            }
        };
    }
}

impl Expression {
    pub fn evaluate(&self, symbols: &mut SStack) -> Result<Value, String> {
        match self {
            Expression::StringLiteral(s) => return Ok(Value::Str(s.t.to_string())),
            Expression::IntLiteral(i) => return Ok(Value::Int32(i.t.parse::<i32>().unwrap())),
            Expression::StructLiteral(fields) => {
                let mut hash = Fields::new();
                for f in &fields.assigns {
                    hash.insert(
                        f.unwrap().name.clone().unwrap().t.clone(),
                        f.unwrap().expression.clone().unwrap().evaluate(symbols)?,
                    );
                }
                return Ok(Value::Strct(hash));
            }
            Expression::Multiply(a, b) => {
                let aa = a.unwrap().evaluate(symbols)?.get_str()?;
                let bb = b.unwrap().evaluate(symbols)?.get_str()?;
                let concat = [aa, bb].concat();
                return Ok(Value::Str(concat));
            }
            Expression::Plus(a, b) => {
                let aa = a.unwrap().evaluate(symbols)?.get_str()?;
                let bb = b.unwrap().evaluate(symbols)?.get_str()?;
                let concat = [aa, bb].concat();
                return Ok(Value::Str(concat));
            }
            Expression::Identifier(a) => {
                let s = symbols.deep_find(&a.t);
                //println!("found identifier {:?}", s);
                return s;
            }
            Expression::Call(name, args) => {
                let n = name.unwrap().evaluate(symbols)?;
                let evaluated_args: Result<Vec<_>, _> = args
                    .into_iter()
                    .map(|a| a.unwrap().evaluate(symbols))
                    .collect();
                let valid_args = evaluated_args?;
                let res = match n {
                    Value::LibFn("print") => print(&valid_args),
                    Value::LibFn("exec") => exec(&valid_args),
                    Value::UserFn(f) => f.evaluate(valid_args, symbols)?,
                    _ => return Err("not callable".to_string()),
                };
                //println!("fun call, result is {:?}", res);
                return Ok(res);
            }
            Expression::Dot(a, b) => {
                let expr = a.unwrap().evaluate(symbols)?;
                let unknow_field_format = format!("unknown field {}", b);
                return match expr {
                    Value::Strct(fields) => fields
                        .get(&b.t)
                        .map(|x| x.clone())
                        .ok_or(unknow_field_format),
                    Value::Str(_s) => {
                        let func_exist = symbols.deep_find(&b.t);
                        return match func_exist {
                            Ok(_x) => {
                                // create fake function with first argument already filled like a closure
                                // UFCS
                                return Err("todo: implement ufcs".to_string());
                            }
                            _ => Err(unknow_field_format),
                        };
                    }
                    Value::UserModule(m) => {
                        let mc = m.module_content.unwrap().clone();
                        let funs = mc.functions();
                        let has_func = funs.iter().find(|f| f.unwrap().name.unwrap().t == *b.t);
                        return match has_func {
                            Some(x) => Ok(Value::UserFn(x.clone().unwrap().clone())),
                            None => {
                                return mc
                                    .modules()
                                    .iter()
                                    .find(|m| m.unwrap().name() == b.t)
                                    .ok_or(format!("symbol {} not found in module {}", b, m.name()))
                                    .map(|m| Value::UserModule(m.unwrap().clone()));
                            }
                        };
                    }
                    _ => Err("not a struct, cant access dot".to_string()),
                };
            }
            Expression::Evaluate(a) => {
                let expr = a.expression.unwrap().evaluate(symbols)?.get_str()?;
                //println!("will evaluate expr: {:?}", expr);
                let parsed = crate::parser::parse_expression(&expr).unwrap();
                return parsed.evaluate(symbols);
            }
        }
    }
}

impl FileModule {
    pub fn evaluate(&self) -> Result<Value, String> {
        let mut stack = new_stack();
        let mut locals = Symbols::new();
        // add lib functions
        locals.insert("print".to_string(), Value::LibFn("print"));
        locals.insert("exec".to_string(), Value::LibFn("exec"));
        // add user defined functions
        let mc = self.module_content.unwrap().clone();
        for f in mc.functions() {
            locals.insert(
                f.unwrap().name.unwrap().t.to_string(),
                Value::UserFn(f.unwrap().clone()),
            );
        }
        // add user defined modules
        for m in mc.modules() {
            locals.insert(
                m.unwrap().name.unwrap().t.to_string(),
                Value::UserModule(m.unwrap().clone()),
            );
        }
        stack.0.push(locals);
        return self
            .get_main()?
            .evaluate([].to_vec(), &mut stack)
            .map(|x| x.clone());
    }
}
