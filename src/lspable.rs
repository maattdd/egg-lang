use crate::ast::*;
use crate::cst::*;
use lspower::lsp::{Diagnostic, DiagnosticSeverity, Position, Range, SemanticToken};

const ST_PARAMETER: u32 = 5;
const ST_TYPE: u32 = 6;
const ST_STRUCT: u32 = 7;
const ST_MODULE: u32 = 8;

pub trait LSPable {
    fn to_diagnostics(&self) -> Vec<Diagnostic>;
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken>;
    fn get_first_token(&self) -> Token;
    fn get_last_token(&self) -> Token;
    fn start_line(&self) -> u32 {
        return self.get_first_token().start_line();
    }
    fn start_pos(&self) -> u32 {
        return self.get_first_token().start_pos();
    }
    fn end_line(&self) -> u32 {
        return self.get_last_token().end_line();
    }
    fn end_pos(&self) -> u32 {
        return self.get_last_token().end_pos();
    }
}

impl LSPable for Token {
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        return [self.to_semantic_token()].to_vec();
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        let mut ret = Vec::new();
        if let Kind::Error(x) = &self.kind {
            ret.push(Diagnostic {
                range: Range {
                    start: Position {
                        line: self.start_line(),
                        character: self.start_pos(),
                    },
                    end: Position {
                        line: self.end_line(),
                        character: self.end_pos(),
                    },
                },
                code: None,
                code_description: None,
                source: None,
                message: x.to_string(),
                related_information: None,
                tags: None,
                data: None,
                severity: Some(DiagnosticSeverity::Error),
            });
        }
        return ret;
    }
    fn get_first_token(&self) -> Token {
        panic!("no reason");
    }
    fn get_last_token(&self) -> Token {
        panic!("no reason");
    }
    fn start_line(&self) -> u32 {
        return self.begin_line;
    }
    fn start_pos(&self) -> u32 {
        return self.begin_pos;
    }
    fn end_line(&self) -> u32 {
        return self.end_line;
    }
    fn end_pos(&self) -> u32 {
        return self.end_pos;
    }
}

#[derive(Debug, Clone)]
pub struct PreSemanticToken {
    line: u32,
    pos: u32,
    length: u32,
    token_type: u32,
}

impl Token {
    fn to_semantic_token(&self) -> PreSemanticToken {
        let token_type = match self.kind {
            Kind::Return | Kind::Func | Kind::Module | Kind::Struct => 0,
            Kind::Id => 1,
            Kind::IntegerLiteral => 2,
            Kind::StringLiteral => 3,
            Kind::Comment => 4,
            _ => 999,
        };
        return PreSemanticToken {
            line: self.begin_line,
            pos: self.begin_pos,
            length: self.size,
            token_type,
        };
    }
}

impl<T: LSPable> LSPable for Parsed<T> {
    fn get_first_token(&self) -> Token {
        return match self {
            Parsed::Valid(x, _) => x.get_first_token(),
            Parsed::Invalid(_, swallowed) => swallowed[0].clone(),
            Parsed::InvalidEOF => panic!("invalid"),
        };
    }
    fn get_last_token(&self) -> Token {
        return match self {
            Parsed::Valid(x, _) => x.get_last_token(),
            Parsed::Invalid(_, swallowed) => swallowed.last().unwrap().clone(),
            Parsed::InvalidEOF => panic!("invalid"),
        };
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        return match self {
            Parsed::Valid(x, comments) => {
                let mut xx = x.to_semantic_tokens();
                for c in comments {
                    xx.push(c.to_semantic_token());
                }
                xx
            }
            Parsed::Invalid(_, swallowed) => {
                swallowed.iter().map(|x| x.to_semantic_token()).collect()
            }
            Parsed::InvalidEOF => [].to_vec(),
        };
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        return match self {
            Parsed::InvalidEOF => [].to_vec(), //todo
            Parsed::Valid(x, _) => x.to_diagnostics(),
            Parsed::Invalid(err, _) => [Diagnostic {
                range: Range {
                    start: Position {
                        line: self.start_line(),
                        character: self.start_pos(),
                    },
                    end: Position {
                        line: self.end_line(),
                        character: self.end_pos(),
                    },
                },
                code: None,
                code_description: None,
                source: None,
                message: err.to_string(),
                related_information: None,
                tags: None,
                data: None,
                severity: Some(DiagnosticSeverity::Error),
            }]
            .to_vec(),
        };
    }
}

impl LSPable for Module {
    fn get_first_token(&self) -> Token {
        return self.module_keyword.get_first_token();
    }
    fn get_last_token(&self) -> Token {
        return self.right_bracket.get_last_token();
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        let mut ret = Vec::new();
        ret.append(&mut self.module_keyword.to_semantic_tokens());
        let mut name = self.name.to_semantic_tokens();
        name[0].token_type = ST_MODULE;
        ret.append(&mut name);
        ret.append(&mut self.left_bracket.to_semantic_tokens());
        ret.append(&mut self.module_content.to_semantic_tokens());
        ret.append(&mut self.right_bracket.to_semantic_tokens());
        return ret;
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        let mut ret = Vec::new();
        ret.append(&mut self.module_keyword.to_diagnostics());
        ret.append(&mut self.name.to_diagnostics());
        ret.append(&mut self.left_bracket.to_diagnostics());
        ret.append(&mut self.module_content.to_diagnostics());
        ret.append(&mut self.right_bracket.to_diagnostics());
        return ret;
    }
}

impl LSPable for ModuleElement {
    fn get_first_token(&self) -> Token {
        return match self {
            ModuleElement::Function(x) => x.get_first_token(),
            ModuleElement::Struct(x) => x.get_first_token(),
            ModuleElement::Module(x) => x.get_first_token(),
        };
    }
    fn get_last_token(&self) -> Token {
        return match self {
            ModuleElement::Function(x) => x.get_last_token(),
            ModuleElement::Struct(x) => x.get_last_token(),
            ModuleElement::Module(x) => x.get_last_token(),
        };
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        return match self {
            ModuleElement::Function(x) => x.to_semantic_tokens(),
            ModuleElement::Struct(x) => x.to_semantic_tokens(),
            ModuleElement::Module(x) => x.to_semantic_tokens(),
        };
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        return match self {
            ModuleElement::Function(x) => x.to_diagnostics(),
            ModuleElement::Struct(x) => x.to_diagnostics(),
            ModuleElement::Module(x) => x.to_diagnostics(),
        };
    }
}

impl LSPable for ModuleContent {
    fn get_first_token(&self) -> Token {
        return self.elements[0].get_first_token();
    }
    fn get_last_token(&self) -> Token {
        return self.elements.last().unwrap().get_last_token();
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        let mut ret = Vec::new();
        ret.append(
            &mut self
                .elements
                .iter()
                .map(|f| f.to_semantic_tokens())
                .flatten()
                .collect(),
        );
        return ret;
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        let mut ret = Vec::new();
        ret.append(
            &mut self
                .elements
                .iter()
                .map(|f| f.to_diagnostics())
                .flatten()
                .collect(),
        );
        return ret;
    }
}

impl LSPable for Struct {
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        return Vec::new();
    }
    fn get_first_token(&self) -> Token {
        return self.keyword.get_first_token();
    }
    fn get_last_token(&self) -> Token {
        return self.right_bracket.get_last_token();
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        let mut ret = Vec::new();
        let keyword = &mut self.keyword.to_semantic_tokens();
        ret.append(keyword);
        let name = &mut self.name.to_semantic_tokens();
        name[0].token_type = ST_STRUCT;
        ret.append(name);
        ret.append(&mut self.left_bracket.to_semantic_tokens());
        ret.append(&mut self.right_bracket.to_semantic_tokens());
        return ret;
    }
}
impl LSPable for Parameter {
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        return Vec::new();
    }
    fn get_first_token(&self) -> Token {
        return self.name.get_first_token();
    }
    fn get_last_token(&self) -> Token {
        return self.ttype.get_last_token();
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        let mut ret = Vec::new();
        let name = &mut self.name.to_semantic_tokens();
        name[0].token_type = ST_PARAMETER;
        ret.append(name);
        ret.append(&mut self.colon.to_semantic_tokens());
        let ttype = &mut self.ttype.to_semantic_tokens();
        ttype[0].token_type = ST_TYPE;
        ret.append(ttype);
        if let Some(x) = &self.comma {
            ret.append(&mut x.to_semantic_tokens());
        }
        return ret;
    }
}
impl LSPable for Function {
    fn get_first_token(&self) -> Token {
        return self.func_symbol.unwrap();
    }
    fn get_last_token(&self) -> Token {
        return self.right_bracket.unwrap();
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        let mut ret = Vec::new();
        ret.append(&mut self.func_symbol.to_semantic_tokens());
        ret.append(&mut self.name.to_semantic_tokens());
        ret.append(&mut self.left_parenthesis.to_semantic_tokens());
        ret.append(&mut self.left_parenthesis.to_semantic_tokens());
        ret.append(
            &mut self
                .parameters
                .iter()
                .map(|f| f.to_semantic_tokens())
                .flatten()
                .collect(),
        );
        ret.append(&mut self.right_parenthesis.to_semantic_tokens());
        ret.append(&mut self.left_bracket.to_semantic_tokens());
        let mut fns = self
            .statements
            .iter()
            .map(|f| f.to_semantic_tokens())
            .flatten()
            .collect();
        ret.append(&mut fns);
        ret.append(&mut self.right_bracket.to_semantic_tokens());
        return ret;
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        let mut ret = Vec::new();
        ret.append(&mut self.func_symbol.to_diagnostics());
        ret.append(&mut self.name.to_diagnostics());
        ret.append(&mut self.left_parenthesis.to_diagnostics());
        ret.append(&mut self.left_parenthesis.to_diagnostics());
        ret.append(
            &mut self
                .parameters
                .iter()
                .map(|f| f.to_diagnostics())
                .flatten()
                .collect(),
        );
        ret.append(&mut self.right_parenthesis.to_diagnostics());
        ret.append(&mut self.left_bracket.to_diagnostics());
        let mut fns = self
            .statements
            .iter()
            .map(|f| f.to_diagnostics())
            .flatten()
            .collect();
        ret.append(&mut fns);
        ret.append(&mut self.right_bracket.to_diagnostics());
        return ret;
    }
}

impl LSPable for Statement {
    fn get_first_token(&self) -> Token {
        return match self {
            Statement::Return(r, _, _) => r.unwrap(),
            Statement::Void(exp, _) => exp.get_first_token(),
            Statement::StmtAssign(a, _) => a.get_first_token(),
        };
    }
    fn get_last_token(&self) -> Token {
        return match self {
            Statement::Return(_, _, semicolon) => semicolon.unwrap(),
            Statement::Void(_, semicolon) => semicolon.unwrap(),
            Statement::StmtAssign(_, semicolon) => semicolon.unwrap(),
        };
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        let mut r = Vec::new();
        match self {
            Statement::Return(ret, exp, semicolon) => {
                r.append(&mut ret.to_semantic_tokens());
                r.append(&mut exp.to_semantic_tokens());
                r.append(&mut semicolon.to_semantic_tokens());
            }
            Statement::Void(exp, semicolon) => {
                r.append(&mut exp.to_semantic_tokens());
                r.append(&mut semicolon.to_semantic_tokens());
            }
            Statement::StmtAssign(assign, semicolon) => {
                r.append(&mut assign.to_semantic_tokens());
                r.append(&mut semicolon.to_semantic_tokens());
            }
        };
        return r;
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        let mut r = Vec::new();
        match self {
            Statement::Return(ret, exp, semicolon) => {
                r.append(&mut ret.to_diagnostics());
                r.append(&mut exp.to_diagnostics());
                r.append(&mut semicolon.to_diagnostics());
            }
            Statement::Void(exp, semicolon) => {
                r.append(&mut exp.to_diagnostics());
                r.append(&mut semicolon.to_diagnostics());
            }
            Statement::StmtAssign(assign, semicolon) => {
                r.append(&mut assign.to_diagnostics());
                r.append(&mut semicolon.to_diagnostics());
            }
        };
        return r;
    }
}

impl LSPable for StructLiteralExpression {
    fn get_first_token(&self) -> Token {
        panic!("todo first token expression");
    }
    fn get_last_token(&self) -> Token {
        panic!("todo last token expression");
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        let mut r = Vec::new();
        r.append(&mut self.left_bracket.to_semantic_tokens());
        let mut assigns = self
            .assigns
            .iter()
            .map(|f| f.to_semantic_tokens())
            .flatten()
            .collect();
        r.append(&mut assigns);
        r.append(&mut self.right_bracket.to_semantic_tokens());
        return r;
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        return Vec::new();
    }
}

impl LSPable for Expression {
    fn get_first_token(&self) -> Token {
        panic!("todo first token expression");
    }
    fn get_last_token(&self) -> Token {
        panic!("todo last token expression");
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        return match self {
            Expression::Identifier(i) => {
                let s = i.to_semantic_token();
                [s].to_vec()
            }
            Expression::Plus(a, b) => {
                let mut ret = Vec::new();
                ret.append(&mut a.to_semantic_tokens());
                // add + token
                ret.append(&mut b.to_semantic_tokens());
                return ret;
            }
            Expression::Multiply(a, b) => {
                let mut ret = Vec::new();
                ret.append(&mut a.to_semantic_tokens());
                // add + token
                ret.append(&mut b.to_semantic_tokens());
                return ret;
            }
            Expression::Dot(a, b) => {
                let mut ret = Vec::new();
                ret.append(&mut a.to_semantic_tokens());
                // add . token
                ret.append(&mut b.to_semantic_tokens());
                return ret;
            }
            Expression::Call(a, b) => {
                let mut ret = Vec::new();
                ret.append(&mut a.to_semantic_tokens());
                let mut args: Vec<PreSemanticToken> =
                    b.iter().map(|f| f.to_semantic_tokens()).flatten().collect();
                ret.append(&mut args);
                return ret;
            }
            Expression::StructLiteral(i) => {
                return i.to_semantic_tokens();
            }
            Expression::Evaluate(i) => {
                return i.expression.to_semantic_tokens();
            }
            Expression::IntLiteral(i) => {
                let s = i.to_semantic_token();
                [s].to_vec()
            }
            Expression::StringLiteral(i) => {
                let s = i.to_semantic_token();
                [s].to_vec()
            }
        };
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        return Vec::new();
    }
}

impl LSPable for Assign {
    fn get_first_token(&self) -> Token {
        panic!("todo first token assign");
    }
    fn get_last_token(&self) -> Token {
        panic!("todo last token assign");
    }
    fn to_semantic_tokens(&self) -> Vec<PreSemanticToken> {
        let mut r = Vec::new();
        r.append(&mut self.name.to_semantic_tokens());
        r.append(&mut self.assign_symbol.to_semantic_tokens());
        r.append(&mut self.expression.to_semantic_tokens());
        return r;
    }
    fn to_diagnostics(&self) -> Vec<Diagnostic> {
        let mut r = Vec::new();
        r.append(&mut self.name.to_diagnostics());
        r.append(&mut self.assign_symbol.to_diagnostics());
        r.append(&mut self.expression.to_diagnostics());
        return r;
    }
}

pub fn compute_deltas(tokens: Vec<PreSemanticToken>) -> Vec<SemanticToken> {
    let mut ret = Vec::new();
    let mut last_line = 0;
    let mut last_pos = 0;
    for token in tokens.iter() {
        let delta_line = token.line - last_line;
        if delta_line > 0 {
            last_pos = 0;
        }
        let delta_pos = token.pos - last_pos;
        // store new semantic token
        ret.push(SemanticToken {
            delta_line,
            delta_start: delta_pos,
            length: token.length,
            token_type: token.token_type,
            token_modifiers_bitset: 0,
        });
        // next step
        last_line = token.line;
        last_pos = token.pos;
    }
    return ret;
}
