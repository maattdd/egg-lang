use logos::Logos;

#[derive(Logos, Debug, PartialEq, Eq, Clone, Hash)]
pub enum Token {
    // RESERVED KEYWORDS
    #[token("module")]
    Module,
    #[token("func")]
    Func,
    #[token("return")]
    Return,
    #[token("struct")]
    Struct,

    // SPACE AND NEWLINE
    #[token("\n")]
    NewLine,
    #[regex(r"([^\S\n])")]
    Space,

    // CHARACTERS
    #[token("/*")]
    BeginMultilineComment,
    #[token("*/")]
    EndMultilineComment,
    #[token("{")]
    LeftBracket,
    #[token("}")]
    RightBracket,
    #[token("(")]
    LeftParenthesis,
    #[token(")")]
    RightParenthesis,
    #[token("<")]
    LessThan,
    #[token(">")]
    MoreThan,
    #[token("+")]
    Plus,
    #[token("*")]
    Star,
    #[token(":=")]
    Assign,
    #[token("#")]
    Sharp,
    #[token(";")]
    Semicolon,
    #[token(",")]
    Comma,
    #[token(":")]
    Colon,
    #[token(".")]
    Dot,

    // LITERALS
    #[regex(r"//[^\n\r]*")]
    LineComment,
    #[regex(r"\d+")]
    IntegerLiteral,
    #[regex(r#""(?:[^"\\]|\\.)*""#)]
    StringLiteral,
    #[regex(r"[a-zA-Z]+")]
    Id,
    #[regex(r"~[a-zA-Z]+")]
    NamedId,

    // ERRORS
    #[regex(r".", priority = 0)]
    Anything,
    #[error]
    Error,
}

#[test]
fn test_lexer() {
    let mut lex = Token::lexer("module // test \n /*&\n*/");
    assert_eq!(lex.next(), Some(Token::Module));
    assert_eq!(lex.span(), 0..6);
    assert_eq!(lex.slice(), "module");
    assert_eq!(lex.next(), Some(Token::Space));
    assert_eq!(lex.next(), Some(Token::LineComment));
    assert_eq!(lex.next(), Some(Token::NewLine));
    assert_eq!(lex.next(), Some(Token::Space));
    assert_eq!(lex.next(), Some(Token::BeginMultilineComment));
    assert_eq!(lex.next(), Some(Token::Anything));
    assert_eq!(lex.next(), Some(Token::NewLine));
    assert_eq!(lex.next(), Some(Token::EndMultilineComment));
}
