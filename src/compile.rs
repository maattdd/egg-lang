use crate::ast::*;
use crate::evaluate::new_stack;

impl<T: Compilable> Compilable for Parsed<T> {
    fn compile(&self) -> Result<String, String> {
        return match self {
            Parsed::Valid(x, _) => x.compile(),
            Parsed::Invalid(_, _) | Parsed::InvalidEOF => {
                Err("can't compiled invalid ast".to_string())
            }
        };
    }
}
impl Compilable for Function {
    fn compile(&self) -> Result<String, String> {
        let mut res = "func ".to_string();
        res += &self.name.unwrap().t.to_string();
        res += &" (".to_string();
        res += &"){".to_string();
        for s in &self.statements {
            match s {
                Parsed::Valid(Statement::Return(_, r, _), _) => {
                    res += "return ";
                    let rr = r.unwrap().compile()?;
                    res += &rr;
                }
                Parsed::Valid(Statement::StmtAssign(_assign, _), _) => {}
                Parsed::Valid(Statement::Void(_e, _), _) => {}
                Parsed::Invalid(_, _) | Parsed::InvalidEOF => panic!("cant compile invalid ast"),
            };
            res += ";";
        }
        res += &"}".to_string();
        return Ok(res);
    }
}

impl Compilable for Expression {
    fn compile(&self) -> Result<String, String> {
        match self {
            Expression::StringLiteral(s) => {
                return Ok(s.t.to_string());
            }
            Expression::IntLiteral(i) => {
                return Ok(i.t.to_string());
            }
            Expression::Evaluate(s) => {
                let mut stack = new_stack();
                let evaluated = s.expression.unwrap().evaluate(&mut stack)?.get_str()?;
                let parsed = crate::parser::parse_expression(&evaluated);
                return parsed.compile();
            }
            _ => panic!("TODO"),
        };
    }
}

pub fn compile_module(m: FileModule) -> Result<(), String> {
    let name = m.name.clone();
    use std::fs;
    let mut str = format!("package {}\n", name);
    let main = m.get_main()?;
    println!("{:?}", main);
    str += main.compile()?.as_str();
    str += "\n";
    fs::write(name + ".go", str).expect("unable to write");
    return Ok(());
}
