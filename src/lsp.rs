use crate::ast::*;
use crate::format::Formatable;
use crate::lspable::*;

use lspower::jsonrpc::Result;
use lspower::lsp::*;
use lspower::{Client, LanguageServer, LspService, Server};
use serde_json::Value;
use std::collections::HashMap;
use tokio::sync::Mutex;

#[derive(Debug)]
struct State {
    sources: HashMap<String, String>,
    asts: HashMap<String, FileModule>,
}

#[derive(Debug)]
struct Backend {
    client: Client,
    state: Mutex<State>,
}

#[lspower::async_trait]
impl LanguageServer for Backend {
    async fn initialize(&self, _: InitializeParams) -> Result<InitializeResult> {
        Ok(InitializeResult {
            server_info: None,
            capabilities: ServerCapabilities {
                hover_provider: Some(HoverProviderCapability::Simple(true)),
                document_formatting_provider: Some(OneOf::Left(true)),
                semantic_tokens_provider: Some(
                    SemanticTokensServerCapabilities::SemanticTokensOptions(
                        SemanticTokensOptions {
                            legend: SemanticTokensLegend {
                                token_types: vec![
                                    SemanticTokenType::KEYWORD,
                                    SemanticTokenType::FUNCTION,
                                    SemanticTokenType::NUMBER,
                                    SemanticTokenType::STRING,
                                    SemanticTokenType::COMMENT,
                                    SemanticTokenType::PARAMETER,
                                    SemanticTokenType::TYPE,
                                    SemanticTokenType::STRUCT,
                                    SemanticTokenType::NAMESPACE,
                                ],
                                token_modifiers: vec![],
                            },
                            range: Some(false),
                            work_done_progress_options: WorkDoneProgressOptions {
                                work_done_progress: None,
                            },
                            full: Some(SemanticTokensFullOptions::Bool(true)),
                        },
                    ),
                ),
                text_document_sync: Some(TextDocumentSyncCapability::Kind(
                    TextDocumentSyncKind::Full,
                )),
                completion_provider: Some(CompletionOptions {
                    resolve_provider: Some(false),
                    trigger_characters: Some(vec![".".to_string()]),
                    work_done_progress_options: WorkDoneProgressOptions {
                        work_done_progress: None,
                    },
                    all_commit_characters: None,
                }),
                execute_command_provider: Some(ExecuteCommandOptions {
                    commands: vec!["dummy.do_something".to_string()],
                    work_done_progress_options: Default::default(),
                }),
                workspace: Some(WorkspaceServerCapabilities {
                    workspace_folders: Some(WorkspaceFoldersServerCapabilities {
                        supported: Some(true),
                        change_notifications: Some(OneOf::Left(true)),
                    }),
                    file_operations: None,
                }),
                ..ServerCapabilities::default()
            },
        })
    }

    async fn hover(&self, params: HoverParams) -> Result<Option<Hover>> {
        self.client
            .log_message(MessageType::Info, format!("hover! {:?}", params))
            .await;
        Ok(Some(Hover {
            contents: HoverContents::Array(vec![MarkedString::String("Todo".to_string())]),
            range: None,
        }))
    }

    async fn initialized(&self, _: InitializedParams) {
        self.client
            .log_message(MessageType::Info, "initialized!")
            .await;
    }

    async fn shutdown(&self) -> Result<()> {
        Ok(())
    }

    async fn did_change_workspace_folders(&self, _: DidChangeWorkspaceFoldersParams) {
        self.client
            .log_message(MessageType::Info, "workspace folders changed!")
            .await;
    }

    async fn did_change_configuration(&self, _: DidChangeConfigurationParams) {
        self.client
            .log_message(MessageType::Info, "configuration changed!")
            .await;
    }

    async fn did_change_watched_files(&self, _: DidChangeWatchedFilesParams) {
        self.client
            .log_message(MessageType::Info, "watched files have changed!")
            .await;
    }

    async fn execute_command(&self, _: ExecuteCommandParams) -> Result<Option<Value>> {
        self.client
            .log_message(MessageType::Info, "command executed!")
            .await;

        match self.client.apply_edit(WorkspaceEdit::default(), None).await {
            Ok(res) if res.applied => self.client.log_message(MessageType::Info, "applied").await,
            Ok(_) => self.client.log_message(MessageType::Info, "rejected").await,
            Err(err) => self.client.log_message(MessageType::Error, err).await,
        }

        Ok(None)
    }

    async fn did_open(&self, params: DidOpenTextDocumentParams) {
        let mut state = self.state.lock().await;
        update_state(
            &mut state,
            params.text_document.uri.path().to_string(),
            params.text_document.text.clone(),
        );
        let diags = get_diagnostics(&state, params.text_document.uri.path().to_string());
        self.client
            .log_message(MessageType::Info, format!("file opened! {:?}", params))
            .await;
        self.client
            .publish_diagnostics(params.text_document.uri, diags, None)
            .await;
    }

    async fn formatting(&self, params: DocumentFormattingParams) -> Result<Option<Vec<TextEdit>>> {
        self.client
            .log_message(MessageType::Info, format!("formatting! {:?}", params))
            .await;
        let mut edits = Vec::new();
        let state = self.state.lock().await;
        let formatted = get_formatted(&state, params.text_document.uri.path().to_string());
        let te = TextEdit {
            range: Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 999,
                    character: 999,
                },
            },
            new_text: formatted,
        };
        edits.push(te);
        Ok(Some(edits))
    }

    async fn did_change(&self, params: DidChangeTextDocumentParams) {
        let mut state = self.state.lock().await;
        let content = params.content_changes[0].text.clone();
        update_state(
            &mut state,
            params.text_document.uri.path().to_string(),
            content,
        );
        self.client
            .log_message(MessageType::Info, format!("file changed! {:?}", params))
            .await;
        let diags = get_diagnostics(&state, params.text_document.uri.path().to_string());
        self.client
            .publish_diagnostics(params.text_document.uri, diags, None)
            .await;
    }

    async fn did_save(&self, _params: DidSaveTextDocumentParams) {
        self.client
            .log_message(MessageType::Info, "file saved!")
            .await;
    }

    async fn did_close(&self, _: DidCloseTextDocumentParams) {
        self.client
            .log_message(MessageType::Info, "file closed!")
            .await;
    }

    async fn completion(&self, _: CompletionParams) -> Result<Option<CompletionResponse>> {
        Ok(Some(CompletionResponse::Array(vec![
            CompletionItem::new_simple("Hello".to_string(), "Some detail".to_string()),
            CompletionItem::new_simple("Bye".to_string(), "More detail".to_string()),
        ])))
    }

    async fn semantic_tokens_full(
        &self,
        params: SemanticTokensParams,
    ) -> Result<Option<SemanticTokensResult>> {
        self.client
            .log_message(
                MessageType::Info,
                format!("semantic token full! {:?}", params),
            )
            .await;
        let mut state = self.state.lock().await;
        let tokens = get_semantic_tokens(&mut state, params.text_document.uri.path().to_string());

        self.client
            .log_message(MessageType::Info, format!("tokens! {:?}", tokens))
            .await;

        let response = Ok(Some(SemanticTokensResult::Tokens(SemanticTokens {
            result_id: None,
            data: tokens,
        })));

        self.client
            .log_message(MessageType::Info, format!("response! {:?}", response))
            .await;

        return response;
    }
}

fn update_state(state: &mut State, path: String, content: String) {
    let parsed = crate::parser::parse_file_module(&path, &content);
    state.sources.insert(path.clone(), content.to_string());
    state.asts.insert(path, parsed);
}

fn get_diagnostics(state: &State, path: String) -> Vec<Diagnostic> {
    let results = state.asts.get(&path);
    match results {
        None => Vec::new(),
        Some(file) => file.module_content.to_diagnostics(),
    }
}

fn get_formatted(state: &State, path: String) -> String {
    let results = state.asts.get(&path);
    match results {
        None => panic!("unknow file"),
        Some(file) => file.to_format("  "),
    }
}

fn get_semantic_tokens(state: &State, path: String) -> Vec<SemanticToken> {
    let results = state.asts.get(&path);
    match results {
        None => Vec::new(),
        Some(file) => compute_deltas(file.module_content.to_semantic_tokens()),
    }
}

#[tokio::main]
pub async fn main() {
    env_logger::init();

    let stdin = tokio::io::stdin();
    let stdout = tokio::io::stdout();

    let sources = HashMap::new();
    let asts = HashMap::new();
    let state = Mutex::new(State { sources, asts });

    let (service, messages) = LspService::new(|client| Backend { client, state });
    Server::new(stdin, stdout)
        .interleave(messages)
        .serve(service)
        .await;
}
