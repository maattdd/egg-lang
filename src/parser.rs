use crate::ast::*;
use crate::cst::*;
use std::fs;

#[derive(Debug, Clone)]
pub struct Parser {
    idx: usize,
    tokens: Vec<Token>,
}

fn infix_binding_power(op: &Token) -> Option<(u8, u8)> {
    let res = match &op.kind {
        Kind::Plus => (10, 20),
        Kind::Star => (30, 40),
        Kind::Dot => (100, 110),
        _ => return None,
    };
    Some(res)
}

fn postfix_binding_power(op: &Token) -> Option<(u8, ())> {
    let res = match &op.kind {
        Kind::LeftParenthesis => (70, ()),
        _ => return None,
    };
    Some(res)
}

fn prefix_binding_power(op: &Token) -> ((), u8) {
    match &op.kind {
        Kind::Sharp => ((), 1),
        _ => panic!("bad op: {:?}", op),
    }
}

impl Parser {
    fn new(tokens: Vec<Token>) -> Parser {
        return Parser { idx: 0, tokens };
    }

    /*
    fn fake_token_vec(&self) -> Vec<Tk> {
        return [Tk {
            line: 0,
            pos: 0,
            size: 0,
            t: "".to_string(),
            delta_line: 0,
            delta_pos: 0,
        }]
        .to_vec();
    }
    fn is_end(&self) -> bool {
        return self.idx >= self.tokens.len();
    }
    */

    fn make_valid<T>(&mut self, x: T) -> Parsed<T> {
        return Parsed::Valid(x, self.consume_comments());
    }

    fn consume_comments(&mut self) -> Vec<Token> {
        let mut ret = Vec::new();
        loop {
            match self.current_kind() {
                Some(Kind::Comment) => {
                    ret.push(self.current().unwrap().clone());
                    self.advance()
                }
                _ => {
                    break;
                }
            }
        }
        return ret;
    }

    fn consume(&mut self, message: &str, kind: Kind) -> Parsed<Token> {
        let token = self.current().clone();
        return match token {
            None => Parsed::Invalid(
                format!("unexpected end of file, was expecting \"{}\"", message),
                Vec::new(),
            ),
            Some(k) => {
                if k.kind == kind {
                    let v = k.clone();
                    self.advance();
                    Parsed::Valid(v, self.consume_comments())
                } else {
                    Parsed::Invalid(
                        format!("unexpected {}, was expecting {}", k.t, message),
                        [k.clone()].to_vec(),
                    )
                }
            }
        };
    }

    fn maybe_consume(&mut self, kind: Kind) -> Option<Parsed<Token>> {
        return match self.current().clone() {
            None => None,
            Some(token) => {
                if token.kind == kind {
                    let v = token.clone();
                    self.advance();
                    Some(Parsed::Valid(v, self.consume_comments()))
                } else {
                    None
                }
            }
        };
    }

    fn current(&self) -> Option<&Token> {
        return self.tokens.get(self.idx);
    }
    fn current_kind(&self) -> Option<Kind> {
        return self.current().map(|x| x.clone().kind);
    }
    fn advance(&mut self) {
        self.idx += 1;
    }
    fn parse_module(&mut self) -> Parsed<Module> {
        // get module keyword
        let module_keyword = self.consume("module", Kind::Module);
        // get name
        let name = self.consume("module name", Kind::Id);
        // get left bracket
        let left_bracket = self.consume("{", Kind::LeftBracket);
        // get module content
        let module_content = self.parse_module_content();
        // get right bracket
        let right_bracket = self.consume("}", Kind::RightBracket);
        return Parsed::Valid(
            Module {
                module_keyword,
                name,
                left_bracket,
                module_content,
                right_bracket,
            },
            self.consume_comments(),
        );
    }
    fn parse_struct(&mut self) -> Parsed<Struct> {
        let keyword = self.consume("struct", Kind::Struct);
        let name = self.consume("struct name", Kind::Id);
        let left_bracket = self.consume("{", Kind::LeftBracket);
        let right_bracket = self.consume("}", Kind::RightBracket);
        return Parsed::Valid(
            Struct {
                keyword,
                name,
                left_bracket,
                right_bracket,
            },
            self.consume_comments(),
        );
    }
    fn parse_module_content(&mut self) -> Parsed<ModuleContent> {
        let mut elements = Vec::new();

        loop {
            match self.current_kind() {
                Some(Kind::Func) => {
                    elements.push(ModuleElement::Function(Box::new(self.parse_function())));
                }
                Some(Kind::Module) => {
                    elements.push(ModuleElement::Module(Box::new(self.parse_module())));
                }
                Some(Kind::Struct) => {
                    elements.push(ModuleElement::Struct(Box::new(self.parse_struct())));
                }
                Some(Kind::RightBracket) => {
                    break;
                }
                Some(_) => {
                    return Parsed::Invalid(
                        "unexpected token, expecting func".to_string(),
                        [self.current().unwrap().clone()].to_vec(),
                    );
                }
                None => {
                    break;
                }
            }
        }
        return Parsed::Valid(ModuleContent { elements }, self.consume_comments());
    }

    fn parse_parameter(&mut self) -> Parsed<Parameter> {
        let named: bool;
        let named_token = self.maybe_consume(Kind::NamedId);
        let name = match named_token {
            None => {
                named = false;
                self.consume("name", Kind::Id)
            }
            Some(x) => {
                named = true;
                x
            }
        };
        let colon = self.consume(":", Kind::Colon);
        let ttype = self.consume("ttype", Kind::Id);
        let comma = self.maybe_consume(Kind::Comma);
        return Parsed::Valid(
            Parameter {
                named,
                name,
                colon,
                ttype,
                comma,
            },
            self.consume_comments(),
        );
    }
    fn parse_function(&mut self) -> Parsed<Function> {
        let func_symbol = self.consume("func", Kind::Func);
        let name = self.consume("id", Kind::Id);
        let left_parenthesis = self.consume("(", Kind::LeftParenthesis);
        // consume parameters
        let mut parameters: Vec<Parsed<Parameter>> = Vec::new();
        loop {
            match self.current_kind() {
                Some(Kind::RightParenthesis) => {
                    break;
                }
                Some(Kind::Comma) => match parameters.last() {
                    None => {
                        return Parsed::Invalid(
                            "comma in empty parameters".to_string(),
                            [self.current().unwrap().clone()].to_vec(),
                        );
                    }
                    Some(param) => match param.unwrap().comma {
                        Some(_) => {
                            continue;
                        }
                        None => {
                            return Parsed::Invalid(
                                "comma in empty parameters".to_string(),
                                [self.current().unwrap().clone()].to_vec(),
                            );
                        }
                    },
                },
                Some(_) => {
                    let p = self.parse_parameter();
                    parameters.push(p);
                }
                None => return Parsed::InvalidEOF,
            }
        }
        // consume right parenthesis
        let right_parenthesis = self.consume(")", Kind::RightParenthesis);
        // consume left bracket
        let left_bracket = self.consume("{", Kind::LeftBracket);
        // parse statements
        let mut statements = Vec::new();
        loop {
            let stmt = self.parse_statement();
            if let Some(x) = stmt {
                statements.push(x);
            } else {
                break;
            }
        }
        // consume right bracket
        let right_bracket = self.consume("}", Kind::RightBracket);
        // build function
        return Parsed::Valid(
            Function {
                func_symbol,
                name,
                parameters,
                statements,
                left_parenthesis,
                right_parenthesis,
                left_bracket,
                right_bracket,
            },
            self.consume_comments(),
        );
    }

    fn parse_statement(&mut self) -> Option<Parsed<Statement>> {
        return match self.current_kind() {
            Some(Kind::Return) => Some(self.parse_statement_return()),
            Some(Kind::Id) => Some(self.parse_statement_assign()),
            Some(Kind::RightBracket) => None,
            Some(_) => Some(Parsed::Invalid(
                "expecting end of function".to_string(),
                [self.current().unwrap().clone()].to_vec(),
            )),
            None => None,
        };
    }

    fn parse_expression_bp(&mut self, min_bp: u8) -> Parsed<Expression> {
        let mut lhs = self.parse_expression_atom();

        loop {
            let op = match self.current_kind() {
                Some(Kind::Dot)
                | Some(Kind::Plus)
                | Some(Kind::Star)
                | Some(Kind::LeftParenthesis) => self.current().unwrap(),
                _ => break,
                //panic!("bad token: {:?}", t),
            }
            .clone();

            if let Some((l_bp, ())) = postfix_binding_power(&op) {
                if l_bp < min_bp {
                    break;
                }
                self.advance();
                if op.kind == Kind::LeftParenthesis {
                    let rhs = self.parse_expression_bp(0);
                    self.consume("func call should end with )", Kind::RightParenthesis);
                    lhs = self.make_valid(Expression::Call(Box::new(lhs), [rhs].to_vec()));
                }
                continue;
            }

            if let Some((l_bp, r_bp)) = infix_binding_power(&op) {
                if l_bp < min_bp {
                    break;
                }

                self.advance();
                let rhs = self.parse_expression_bp(r_bp);

                lhs = match op.clone().kind.clone() {
                    Kind::Plus => self.make_valid(Expression::Plus(Box::new(lhs), Box::new(rhs))),
                    Kind::Star => {
                        self.make_valid(Expression::Multiply(Box::new(lhs), Box::new(rhs)))
                    }
                    Kind::Dot => {
                        if let Expression::Identifier(i) = rhs.unwrap() {
                            self.make_valid(Expression::Dot(Box::new(lhs), i))
                        } else {
                            Parsed::Invalid(format!("rhs of dot should be id"), [].to_vec())
                        }
                    }
                    _ => panic!("todo"),
                };
                continue;
            }
            break;
        }
        return lhs;
    }
    fn parse_expression(&mut self) -> Parsed<Expression> {
        return self.parse_expression_bp(0);
    }
    fn parse_expression_atom(&mut self) -> Parsed<Expression> {
        let kind = self.current_kind();
        let expr = match kind {
            Some(Kind::StringLiteral) => {
                self.make_valid(Expression::StringLiteral(self.current().unwrap().clone()))
            }
            Some(Kind::IntegerLiteral) => {
                self.make_valid(Expression::IntLiteral(self.current().unwrap().clone()))
            }
            Some(Kind::Id) => {
                self.make_valid(Expression::Identifier(self.current().unwrap().clone()))
            }
            Some(Kind::Sharp) => {
                let sharp = self.current().unwrap().clone();
                let ((), r_bp) = prefix_binding_power(self.current().unwrap());
                self.advance();
                let rhs = self.parse_expression_bp(r_bp);
                let lit = EvaluateExpression {
                    sharp,
                    evaluated: None,
                    expression: Box::new(rhs),
                };
                return self.make_valid(Expression::Evaluate(lit));
            }
            Some(Kind::LeftBracket) => {
                let left_bracket = self.consume("{", Kind::LeftBracket);
                let assign = self.parse_assign();
                let assigns = [assign].to_vec();
                let right_bracket = self.consume("}", Kind::RightBracket);
                let st = StructLiteralExpression {
                    left_bracket,
                    right_bracket,
                    assigns,
                };
                return self.make_valid(Expression::StructLiteral(st));
            }
            Some(_) => {
                return Parsed::Invalid(
                    format!(
                        "expecting atom expression, not {}",
                        self.current().unwrap().t
                    ),
                    [self.current().unwrap().clone()].to_vec(),
                )
            }
            None => Parsed::InvalidEOF,
        };
        self.advance();
        return expr;
    }

    fn parse_statement_return(&mut self) -> Parsed<Statement> {
        // consume return
        let return_t = self.consume("return", Kind::Return);
        // consume expr
        let expr = self.parse_expression();
        // consume semicolon
        let semicolon = self.consume("semicolon", Kind::Semicolon);
        return Parsed::Valid(
            Statement::Return(return_t, expr, semicolon),
            self.consume_comments(),
        );
    }

    fn parse_assign(&mut self) -> Parsed<Assign> {
        let name = self.consume("id", Kind::Id);
        let assign_symbol = self.consume(":=", Kind::Assign);
        let expression = self.parse_expression();
        return self.make_valid(Assign {
            assign_symbol,
            name,
            expression,
        });
    }
    fn parse_statement_assign(&mut self) -> Parsed<Statement> {
        let assign = self.parse_assign();
        let semicolon = self.consume(";", Kind::Semicolon);
        return Parsed::Valid(
            Statement::StmtAssign(assign, semicolon),
            self.consume_comments(),
        );
    }
}

pub fn parse_file_module(name: &str, content: &str) -> FileModule {
    let tokens = process(content);
    let mut parser = Parser::new(tokens);
    let module_content = parser.parse_module_content();

    return FileModule {
        name: name.to_string(),
        module_content,
    };
}

pub fn parse_file(name: &str) -> FileModule {
    let content = fs::read_to_string(name.to_string() + ".egg").unwrap();
    return parse_file_module(name, content.as_str());
}

pub fn parse_expression(input: &str) -> Parsed<Expression> {
    let tokens = process(input);
    let mut parser = Parser::new(tokens);
    return parser.parse_expression();
}

pub fn parse_statement(input: &str) -> Option<Parsed<Statement>> {
    let tokens = process(input);
    let mut parser = Parser::new(tokens);
    return parser.parse_statement();
}

#[test]
fn test_parse_expression() {
    assert_eq!(
        parse_expression("1 + 2 + 3").unwrap().to_sexp(),
        "(+ (+ 1 2) 3)"
    );
    assert_eq!(
        parse_expression("a.b.c").unwrap().to_sexp(),
        "(. (. a b) c)"
    );
    assert_eq!(
        parse_expression("1 + 2 * 3").unwrap().to_sexp(),
        "(+ 1 (* 2 3))"
    );
    assert_eq!(
        parse_expression("vec.map(2)").unwrap().to_sexp(),
        "(call (. vec map) 2)"
    );
    assert_eq!(
        parse_expression("vec.map(2).last").unwrap().to_sexp(),
        "(. (call (. vec map) 2) last)"
    );
    assert_eq!(
        parse_expression("#map(2)").unwrap().to_sexp(),
        "(# (call map 2))"
    );
}

#[test]
fn test_parser() {
    let parsed = parse_file("examples/simple");
    println!("{:?}", parsed);
    assert_eq!("examples/simple", parsed.name);
}
